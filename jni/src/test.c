/*
 * test.c
 *
 *  Created on: May 7, 2014
 *      Author: mutti
 */

#include "test.h"

void usage(char *progname) {
	printf("usage:  %s [-i input_file] [-o output_file] \n", progname);
	exit(1);
}

int main(int argc, char **argv) {

	char *scon, *tcon, *class, *perm;
	char *txtfile_i = NULL, *txtfile_o = NULL;
	int res = -1;
	FILE *infile = NULL;
	FILE *outfile = NULL;
	int ch;
	struct option long_options[] = { { "output", required_argument, NULL, 'o' },
			{ "input", required_argument, NULL, 'i' }, { NULL, 0, NULL, 0 } };

	while ((ch = getopt_long(argc, argv, "i:o:", long_options, NULL)) != -1) {
		switch (ch) {
		case 'i':
			txtfile_i = optarg;
			break;
		case 'o':
			txtfile_o = optarg;
			break;
		case 'h':
		default:
			usage(argv[0]);
		}
	}

	infile = fopen(txtfile_i, "r");
	if (infile == NULL) {
		fprintf(stderr, "Error open input file: '%s'", txtfile_i);
		return -1;
	}

	if (txtfile_o != NULL) {
		outfile = fopen(txtfile_o, "w");
		if (outfile == NULL) {
			fprintf(stderr, "Error open output file: '%s'", txtfile_o);
			return -1;
		}
	} else
		outfile = stdout;

	int i = 0;
	//init variables
	scon = (char*) malloc(50 * sizeof(char));
	tcon = (char*) malloc(50 * sizeof(char));
	class = (char*) malloc(30 * sizeof(char));
	perm = (char*) malloc(30 * sizeof(char));

	while (feof(infile) == 0) {
		res = fscanf(infile, "%s %s %s %s\n", scon, tcon, class, perm);
		res = selinux_check_access(scon, tcon, class, perm, NULL); // KO
		fprintf(outfile, "\n Rule number: %10d, Result %5s", i,
				res == 0 ? "true" : "false");
		i++;
	}

	fprintf(outfile, "\n");
	fclose(infile);
	fclose(outfile);
	free(scon);
	free(tcon);
	free(class);
	free(perm);
	return 0;
}
