LOCAL_PATH:= $(call my-dir)
	
common_cflags := \
	-Wall -Wshadow -O2 \
	-pipe -fno-strict-aliasing \
	-Wno-return-type -DSHARED

common_includes := \
	$(LOCAL_PATH)/include/


include $(CLEAR_VARS)
LOCAL_MODULE := libselinux-prebuilt
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/libselinux.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libc-prebuilt
LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/libc.so
include $(PREBUILT_SHARED_LIBRARY)


##
# avc_test
#
include $(CLEAR_VARS)

LOCAL_MODULE := avc_test
LOCAL_C_INCLUDES := $(common_includes) 
LOCAL_CFLAGS := $(common_cflags)
LOCAL_SRC_FILES := src/test.c
LOCAL_SHARED_LIBRARIES := libc-prebuilt libselinux-prebuilt
LOCAL_MODULE_CLASS := EXECUTABLES

include $(BUILD_EXECUTABLE)
