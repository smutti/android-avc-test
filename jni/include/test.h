/*
 * test.h
 *
 *  Created on: May 7, 2014
 *      Author: mutti
 */

#ifndef TEST_H_
#define TEST_H_

#include <selinux/selinux.h>
#include <selinux/android.h>
#include <selinux/avc.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <getopt.h>

extern char *optarg;
extern int optind;

#endif /* TEST_H_ */
